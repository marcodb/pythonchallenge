#!/usr/bin/python
import re

with open("mess", "r") as inputFile:
    inputString = inputFile.read().replace('\n','')

    print "The solution is: ",
    for match in re.finditer(r"[a-z0-9]{1}[A-Z]{3}[a-z]{1}[A-Z]{3}[a-z0-9]{1}", inputString):
        print match.group(0)[4:5],
