#!/usr/bin/python
import pickle
from pprint import pprint


with open("banner.p", "r") as inputFile:  
    rows = pickle.load(inputFile)
    
    print "The solution is:"
    for row in rows:
        print(''.join(char * times for char,times in row))
