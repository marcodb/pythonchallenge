#!/usr/bin/python
from zipfile import ZipFile

static = "Next nothing is "
nothing = "90052"
notfound = True
comments = []

with ZipFile('channel.zip', 'r') as channelZip:

    while(notfound):
        with channelZip.open(nothing+".txt", 'r') as varfile:
            
            comments.append(channelZip.getinfo(nothing+".txt").comment)

            content = varfile.read()
            if content.find(static) != -1:
                nothing = content.replace(static,"")
            else:
                notfound = False

    print "File content: " + content
    print "Comments: \n" + ''.join(comments)
    
    letters = []
    for comment in comments:
        try:
            letters.index(comment)
            
        except ValueError:
            letters.append(comment)
    
    print "Look into the letters: \n" + ''.join(letters)

