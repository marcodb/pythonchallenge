# Python Challenge solutions

[The Python Challenge](http://www.pythonchallenge.com/)

This repository contains my solutions with Python scripts and related 
resources needed to solve challenges.
