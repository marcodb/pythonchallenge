#!/usr/bin/python

import string
from string import maketrans

inputString = "g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj."

def shiftChar(x) : 
    if(ord(x)+2 < ord('z')):
        return chr(ord(x) + 2)
    else:
        return chr(ord(x) - (ord('z') - ord('a') + 1) + 2) 

inttab = string.ascii_lowercase
outtab = ''.join(map(shiftChar, inttab))

trantab = maketrans(inttab, outtab)

print inputString.translate(trantab)

print "Translated URL : " + 'map'.translate(trantab)


