#!/usr/bin/python

import urllib2

url = "http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing="
fixed = "and the next nothing is "
nothing = ""
html = fixed

while html.startswith(fixed) :
    print "GET: " + url + nothing
    response = urllib2.urlopen(url+nothing)
    html = response.read()
    nothing = html.replace(fixed,"")

print "The solution is: " + html
