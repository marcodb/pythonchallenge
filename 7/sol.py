#!/usr/bin/python
import exifread
from PIL import Image

filename = 'oxygen.png'

with open(filename, 'rb') as f:
    tags = exifread.process_file(f)
    
    print "Exif tags are ",tags

    im = Image.open(f)
    
    print "Image data: ", im.format, im.size, im.mode

    pixels = list(im.getdata())
    print "Pixels number: ",len(pixels)
    
    width, height = im.size

    chars = [chr(pixel[0]) for pixel in [im.getpixel((i,height//2)) for i in range(0,width)] if pixel[0] == pixel[1] == pixel[2]]
    chars = [chars[i] for i in range(0,len(chars),7)]
    
    solution = ''.join(chars)

    print "The solution is: ", ''.join(chars)

    nextWord = [chr(int(num.strip())) for num in solution[solution.index('[')+1:solution.index(']')].split(',')]
    print "The key is: ", ''.join(nextWord)
