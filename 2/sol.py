#!/usr/bin/python
import operator

with open("mess", "r") as inputFile:
    inputString = inputFile.read().replace('\n','')

    charsDict = {}

    for char in inputString :
        if charsDict.has_key(char) :
            charsDict[char] = charsDict[char] + 1
        else:
            charsDict[char] = 1
    
    print "The solution is: ",
    for char in inputString :
        if charsDict[char] == 1 :
            print char,


